package br.com.rubens.violintutor;



import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RegisterActivity extends AppCompatActivity
{

    private UserRegisterTask mAuthTask = null;

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mNameView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mNameView = (AutoCompleteTextView) findViewById(R.id.tv_reg_name);

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.tv_reg_email);
//        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
//        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener()
//        {
//            @Override
//            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent)
//            {
//        if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL)
//        {
////            attemptRegister();
//            return true;
//        }
//        return false;
//            }
//        });

    }



    private void attemptRegister()
    {
        if (mAuthTask != null)
        {
            return;
        }

        // Reset errors.
        mNameView.setError(null);
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

//        // Check for a valid password, if the user entered one.
//        if (!TextUtils.isEmpty(password) && !isPasswordValid(password))
//        {
//            mPasswordView.setError(getString(R.string.error_invalid_password));
//            focusView = mPasswordView;
//            cancel = true;
//        }
//        if (TextUtils.isEmpty(password))
//        {
//            mPasswordView.setError(getString(R.string.error_field_required));
//            focusView = mPasswordView;
//            cancel = true;
//        }
//        // Check for a valid email address.
//        if (TextUtils.isEmpty(email))
//        {
//            mEmailView.setError(getString(R.string.error_field_required));
//            focusView = mEmailView;
//            cancel = true;
//        } else if (!isEmailValid(email))
//        {
//            mEmailView.setError(getString(R.string.error_invalid_email));
//            focusView = mEmailView;
//            cancel = true;
//        }
//
//        if (cancel)
//        {
//            // There was an error; don't attempt login and focus the first
//            // form field with an error.
//            focusView.requestFocus();
//        }
//        else
//        {
//            // Show a progress spinner, and kick off a background task to
//            // perform the user login attempt.
//            showProgress(true);
//            mAuthTask = new UserRegisterTask("name", email, password);
//            mAuthTask.execute((Void) null);
//        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean>
    {
        private final String mEmail;
        private final String mPassword;
        private final String mName;

        UserRegisterTask(String name, String email, String password)
        {
            mName = name;
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            int tmp;
            String data="";
            try
            {
                URL url = new URL("http://ec2-54-233-232-74.sa-east-1.compute.amazonaws.com/register.php");
                String urlParams = "&name="+mName+"&email="+mEmail+"&password="+mPassword;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();
                InputStream is = httpURLConnection.getInputStream();

                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }
                is.close();
                httpURLConnection.disconnect();

                if(data.equals(""))
                {
                    return true;
                }

                return false;

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return false;
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return false;
            }
        }

//        @Override
//        protected void onPostExecute(final Boolean success)
//        {
//            mAuthTask = null;
//            showProgress(false);
//
//            if (success)
//            {
////                finish();
//                mPasswordView.getText().clear();
//                mEmailView.getText().clear();
//                Toast.makeText(ctx,"Usuario Cadastrado", Toast.LENGTH_LONG).show();
//                populateAutoComplete();
//            }
//            else
//            {
//                mPasswordView.setError(getString(R.string.error_incorrect_password));
//                mPasswordView.requestFocus();
//            }
//        }
//
//        @Override
//        protected void onCancelled()
//        {
//            mAuthTask = null;
//            showProgress(false);
//        }
    }


}



